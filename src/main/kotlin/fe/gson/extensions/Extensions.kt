package fe.gson.extensions

import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonParser
import java.io.*


inline fun <reified T : JsonElement> parseInputStreamAs(inputStream: InputStream) =
    parseReaderAs<T>(inputStream.reader())

inline fun <reified T : JsonElement> parseReaderAs(reader: Reader) = reader.use {
    JsonParser.parseReader(it) as T
}

inline fun <reified T : JsonElement> parseFileAs(file: File) = parseReaderAs<T>(file.bufferedReader())

inline fun <reified T : JsonElement> parseStringAs(str: String): T = JsonParser.parseString(str) as T

inline fun <reified T> Gson.streamToInstance(str: InputStream): T =
    readerToInstance(str.bufferedReader())

inline fun <reified T> Gson.readerToInstance(reader: Reader): T = reader.use {
    this.fromJson(it, T::class.java)
}

inline fun <reified T> Gson.fileToInstance(strFile: String) = fileToInstance<T>(File(strFile))

inline fun <reified T> Gson.fileToInstance(file: File) = readerToInstance<T>(file.bufferedReader())

fun Gson.writeElementToFile(element: Any, file: File) = file.bufferedWriter().use {
    it.write(this.toJson(element))
}
