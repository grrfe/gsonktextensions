package fe.gson.extensions

import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.JsonPrimitive

fun JsonElement.asPrim() = this.asJsonPrimitive

fun JsonElement.asString() = this.asPrim().asString
fun JsonElement.asLong() = this.asPrim().asLong
fun JsonElement.asInt() = this.asPrim().asInt
fun JsonElement.asBool() = this.asPrim().asBoolean
fun JsonElement.asDouble() = this.asPrim().asDouble
fun JsonElement.asFloat() = this.asPrim().asFloat
fun JsonElement.asShort() = this.asPrim().asShort
fun JsonElement.asByte() = this.asPrim().asByte
fun JsonElement.asChar() = this.asPrim().asCharacter
fun JsonElement.asBigdecimal() = this.asPrim().asBigDecimal
fun JsonElement.asBigint() = this.asPrim().asBigInteger



