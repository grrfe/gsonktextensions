package fe.gson.extensions

import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.JsonPrimitive


fun JsonObject.obj(key: String) = this.getAsJsonObject(key)
fun JsonObject.keys(fn: (String, JsonElement) -> Unit) {
    this.keySet().forEach {
        fn(it, this.get(it))
    }
}

fun JsonObject.keys(): Map<String, JsonElement> {
    return this.keySet().associateWith { this.get(it) }
}

fun <T : JsonElement> JsonObject.keys(map: (JsonElement) -> T): Map<String, T> {
    return this.keySet().associateWith { map(this.get(it)) }
}

fun JsonObject.single(): JsonElement? {
    if (this.keySet().size == 1) {
        this.keySet().forEach {
            return this.get(it)
        }
    }

    return null
}

fun JsonObject.array(key: String) = this.getAsJsonArray(key)
inline fun <reified T : JsonElement> JsonObject.arrayMapped(key: String): List<T> {
    return this.array(key).map { it as T }
}

fun JsonObject.prim(key: String): JsonPrimitive? {
    val elem = this.get(key)
    return if (elem != null && elem is JsonPrimitive) {
        this.getAsJsonPrimitive(key)
    } else null
}

fun JsonObject.string(key: String) = this.prim(key)?.asString
fun JsonObject.long(key: String) = this.prim(key)?.asLong
fun JsonObject.int(key: String) = this.prim(key)?.asInt
fun JsonObject.bool(key: String) = this.prim(key)?.asBoolean
fun JsonObject.double(key: String) = this.prim(key)?.asDouble
fun JsonObject.float(key: String) = this.prim(key)?.asFloat
fun JsonObject.short(key: String) = this.prim(key)?.asShort
fun JsonObject.byte(key: String) = this.prim(key)?.asByte
fun JsonObject.char(key: String) = this.prim(key)?.asCharacter
fun JsonObject.bigdecimal(key: String) = this.prim(key)?.asBigDecimal
fun JsonObject.bigint(key: String) = this.prim(key)?.asBigInteger



