# GSONKtExtensions

## Deprecation notice

This repository has been moved to [gson-ext](https://gitlab.com/grrfe/gson-ext). Development will continue there. This
repository has been reset to the last commit before the changes which created gson-ext; its sole purpose is to provide
JitPack compatibility as they don't track/cache name changes.
